#include "ppm_io.h"
#include "edges.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "blur.h"
#include "gray.h"


Image * edges(Image *fimg, float sigma, int thresh) {
   
    int rows = fimg->rows;
    int cols = fimg->cols;

    fimg = gray(fimg);
    fimg = blur(fimg, sigma); 
   
    Pixel imgMatrix[rows][cols];
    for (int p = 0; p < rows; p++) {
        for (int q = 0; q < cols; q++) {
            imgMatrix[p][q] = fimg->data[p * cols + q];
        }
    }

    Image * timg = (Image *) malloc (sizeof(Image));
    timg->rows = fimg->rows;
    timg->cols = fimg->cols;
    timg->data = (Pixel *) malloc(fimg->rows * fimg->cols * sizeof(Pixel));

    float mag;
    float Dx;
    float Dy;

    // sets first row to previous img
    for (int i = 0; i < cols; i++) {
        timg->data[i] = fimg->data[i];
    }
    // sets the first and last columns to prevoiu simage 
    for (int i = 0; i < rows; i++) {
        timg->data[i*cols] = fimg->data[i*cols + 1];
    }

    // sets the first and last columns to prevoiu simage
    for (int i = 0; i < rows; i++) {
        timg->data[i*cols + cols - 1] = fimg->data[i*cols + cols - 1];
    }

    for (int i = 1; i < rows-1; i++) {
        for (int j = 1; j < cols-1; j++) {
            Dx = (imgMatrix[i][j+1].r - imgMatrix[i][j-1].r) / 2.0;
            Dy = (imgMatrix[i+1][j].r - imgMatrix[i-1][j].r) / 2.0;
            mag = sqrt(sq(Dx) + sq(Dy));
            if (mag > thresh) {
                timg->data[i*cols + j].r = 0;
                timg->data[i*cols + j].g = 0;
                timg->data[i*cols + j].b = 0;
            }
            else {
                timg->data[i*cols + j].r = 255;
                timg->data[i*cols + j].g = 255;
                timg->data[i*cols + j].b = 255;
            }
       }

    }

    return timg;
}

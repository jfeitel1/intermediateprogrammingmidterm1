#include "ppm_io.h"
#include "swap.h"
#include <stdlib.h>
#include <stdio.h>

Image * swap(Image *fimg) {
    Image * timg = (Image *) malloc (sizeof(Image));
    timg->rows = fimg->rows;
    timg->cols = fimg->cols;
    timg->data = (Pixel *) malloc(fimg->rows * fimg->cols * sizeof(Pixel));
    
    for (int i = 0; i < (fimg->rows * fimg->cols); i++) {
        int r = fimg->data[i].r;
        int g = fimg->data[i].g;
        int b = fimg->data[i].b;
        
        timg->data[i].r = g;
        timg->data[i].g = b;
        timg->data[i].b = r;
    }
    return timg;
}

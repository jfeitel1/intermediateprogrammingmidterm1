#include "ppm_io.h"
#include "bright.h"
#include <stdlib.h>
#include <stdio.h>

Image * bright(Image *fimg, int brightness) {
    Image * timg = (Image *) malloc (sizeof(Image));
    timg->rows = fimg->rows;
    timg->cols = fimg->cols;
    timg->data = (Pixel *) malloc(fimg->rows * fimg->cols * sizeof(Pixel));
    
    for (int i = 0; i < (fimg->rows * fimg->cols); i++) {
        int r = ((int) fimg->data[i].r) + brightness;
        int g = ((int) fimg->data[i].g) + brightness;
        int b = ((int) fimg->data[i].b) + brightness;
        if(r > 255)
            r = 255;
        else if(r < 0)
            r = 0;
        if(g > 255)
            g = 255;
        else if(g < 0)
            g = 0;
        if(b > 255)
            b = 255;
        else if(b < 0)
            b = 0;
        
        timg->data[i].r = r;
        timg->data[i].g = g;
        timg->data[i].b = b;
        
    }
    return timg;
}

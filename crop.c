#include "ppm_io.h"
#include "crop.h"
#include <stdlib.h>
#include <stdio.h>

Image * crop(Image *fimg, int tc, int tr, int bc, int br) {
    Image * timg = (Image *) malloc (sizeof(Image));
    if (bc - tc < 0)
        printf("Error bottom col less than top col");
    if (br - tr < 0)
        printf("Error bottom row less than top row");

    timg->rows = br - tr;
    timg->cols = bc - tc;
    timg->data = (Pixel *) malloc( (br - tr) * (bc - tc) * sizeof(Pixel));
    printf("Created new img of size %d\n", (bc - tc) * (br - tr));
    int count = 0;
    for (int i = tr; i < br; i++) {
        for (int j = tc; j < bc; j++) {
            
            timg->data[count] = fimg->data[i * fimg->cols + j];
            count += 1;
        }
   }
   printf("count: %d", count);


    return timg;
}

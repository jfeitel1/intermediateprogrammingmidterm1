#include "ppm_io.h"
#include "invert.h"
#include <stdlib.h>
#include <stdio.h>

Image * invert(Image *fimg) {
    Image * timg = (Image *) malloc (sizeof(Image));
    timg->rows = fimg->rows;
    timg->cols = fimg->cols;
    timg->data = (Pixel *) malloc(fimg->rows * fimg->cols * sizeof(Pixel));
    
    for (int i = 0; i < (fimg->rows * fimg->cols); i++) {
        timg->data[i].r = 255 - fimg->data[i].r;
        timg->data[i].g = 255 - fimg->data[i].g;
        timg->data[i].b = 255 - fimg->data[i].b;
    }
    return timg;
}

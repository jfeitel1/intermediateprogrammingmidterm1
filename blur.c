#include "ppm_io.h"
#include "blur.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

float sq(float f) {
    return f*f;
}

//creates the filter matrix to apply to the image
void gaussianFilter(float sigma, int N, float **filter) {

    double g;
    int dx;
    int dy;
    const float PI = 3.14159265358979323846;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            dx = abs( (N / 2) - j);
            dy = abs( (N / 2) - i);
            
            g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
            filter[i][j] = g;
        }
    }
}

Pixel* applyFilter(Pixel **imgMatrix, float **filter, int rows, int cols, int i, int j, int N) {
    // Here N = sigma and n = (sigma - 1) / 2   EXAMPLE: if N = 11 then n = 5
    int  n = N / 2;
    int tr; // top row
    int br; // bottom row
    int tc; //top col
    int bc; // bottom col

    // i and j are the row and col of the actual pixel
    // following lines calculate the start and end indexes of the FOR loop for the Gaussian Matrix 
    //
    if (i - n < 0) { 
        tr = n - i;
    }
    else { 
        tr = 0;
    }

    if (i + n > rows-1) {
        br = N  - ( (i + n) - (rows - 1) );
        }
    else {
        br = N;
    }
    if (j - n < 0) {
        tc = n - j;
    }
    else {
        tc = 0;
    }
    if (j + n > cols-1) {
        bc = N  - ( (j + n) - (cols - 1) );
        }
    else {
        bc = N;
    }


    float sum_r = 0;
    float sum_g = 0;
    float sum_b = 0;
    
    int k = 0;
    int l = 0;
    float count = 0;


    //we take the original pixel (i,j) and the subtract or add the offset calculated previously for the Gaussian
    //matrix to take care of the edge cases, then it sums up the adjusted values and returns a new pixel
    for (int p = i - (n - tr); p < i + (br - n); p++, k++) {
        l = 0;
        for (int q = j - (n - tc); q < j + (bc - n); q++, l++) {
            
            sum_r += imgMatrix[p][q].r * filter[tr + k][tc + l];
            sum_g += imgMatrix[p][q].g * filter[tr + k][tc + l];
            sum_b += imgMatrix[p][q].b * filter[tr + k][tc + l];
            count += filter[tr + k][tc + l];
        }
    }
    Pixel* p =  (Pixel *) malloc(sizeof(Pixel));
    p->r = sum_r / (float) count;
    p->g = sum_g / (float) count;
    p->b = sum_b / (float) count;
   return p ;
}    


Image * blur(Image *fimg, float sigma) {
    int N;
    int rows = fimg->rows;
    int cols = fimg->cols;
    //converts the sigma to a proper N value (Example: 0.5 -> 5 and 1 -> 11)
    if ((int) ceil(sigma * 10) % 2 == 0) {
        N = (int) ceil(sigma * 10) + 1;
    }
    else {
        N = (int) ceil(sigma * 10);
    }
    
    //creates and allocated a filter matrix to store the Gaussian filter values
    float ** filter = (float **)malloc(N * sizeof(float*));
    for (int i = 0; i < N; i++) {
        filter[i] = (float *)malloc(N * sizeof(float));
    }
    //Set the values for the Gaussian Filter matrix
    gaussianFilter(sigma, N, filter);
    
    // creates pixel matrix array to store image to make it easier to work with
    Pixel **imgMatrix = (Pixel **)malloc(rows * sizeof(Pixel*));
    for (int i = 0; i < rows; i++) {
        imgMatrix[i] = (Pixel*)malloc(cols * sizeof(Pixel));
    } 

    for (int p = 0; p < rows; p++) {
        for (int q = 0; q < cols; q++) {
            imgMatrix[p][q] = fimg->data[p * cols + q];
        }
    }
    
    //image to return
    Image * timg = (Image *) malloc (sizeof(Image));
    timg->rows = fimg->rows;
    timg->cols = fimg->cols;
    timg->data = (Pixel *) malloc(fimg->rows * fimg->cols * sizeof(Pixel));

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            Pixel *p = applyFilter(imgMatrix, filter, fimg->rows, fimg->cols, i, j, N);
            timg->data[i * cols + j] =  *p;
            free(p);
       }

    }

    // Free Img Matrix Array
    for (int i = 0; i < rows; i++) {
        free(imgMatrix[i]);
    }
    free(imgMatrix);

    // Free filter array
    for (int i = 0; i < N; i++) {
        free(filter[i]);
    }
    free(filter);
    
    return timg;
}

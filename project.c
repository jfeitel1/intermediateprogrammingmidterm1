#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swap.h"
#include "bright.h"
#include "invert.h"
#include "gray.h"
#include "crop.h"
#include "blur.h"
#include "edges.h"
#include "ppm_io.h"

int main(int argc, char *argv[]) {
    if(argc < 3) {
        return 1;
    }
    
    char *fromFileName = argv[1];
    char *toFileName = argv[2];
    char *oper = argv[3];
    FILE *fromFile = fopen(fromFileName, "rb");
    FILE *toFile = fopen(toFileName, "wb");
    if(fromFile == NULL) {
        if(toFile != NULL)
            fclose(toFile);
        return 2;
    }
    if(toFile == NULL) {
        if(fromFile != NULL)
            fclose(fromFile);
        return 7;
    }
    Image *fromImage = read_ppm(fromFile);
    Image *toImage;
    if(fromImage == NULL) {
        fclose(fromFile);
        fclose(toFile);
        return 3;
    }
    
    // Determine the operation to do
    if(strcmp(oper, "swap") == 0) {
        toImage = swap(fromImage); 
    }
    else if(strcmp(oper, "bright") == 0) {
        if(argc != 5) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 5;
        }
        int brightness = atoi(argv[4]);
        toImage = bright(fromImage, brightness);
    }
    else if(strcmp(oper, "invert") == 0) {
        // Invert the colors of the image
        toImage = invert(fromImage);
    }
    else if(strcmp(oper, "gray") == 0) {
        // Convert the image to grayscale
        toImage = gray(fromImage);
    }
    else if(strcmp(oper, "crop") == 0) {
        if(argc != 8) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 5;
        }

        int tc = atoi(argv[4]);
        int tr = atoi(argv[5]);
        int bc = atoi(argv[6]);
        int br = atoi(argv[7]);
        
        if ( (bc < tc) || (br < tr) || ( (bc == br) && (tc == tr)) ) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 6;
        }

        toImage = crop(fromImage, tc, tr, bc, br);

        
    }
    else if(strcmp(oper, "blur") == 0) {
        if(argc != 5) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 5;
        }

        float sigma = atof(argv[4]);
        if (sigma < 0) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 6;
        }
        toImage = blur(fromImage, sigma);         
    }
    else if(strcmp(oper, "edges") == 0) {
        if(argc != 6) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 5;
        }
        float sigma = atof(argv[4]);
        int thresh = atoi(argv[5]);

        if ((thresh < 0) || (sigma < 0)) {
            free(fromImage->data);
            free(fromImage);
            fclose(fromFile);
            fclose(toFile);
            return 6;
        }
        toImage = edges(fromImage, sigma, thresh);
    }
    else { 
        free(fromImage->data);
        free(fromImage);
        fclose(fromFile);
        fclose(toFile);
        return 4;
    }

    write_ppm(toFile, toImage);

    // Free all dynamically allocated memory, images and pixels
    free(toImage->data);
    free(toImage); 
    free(fromImage->data);
    free(fromImage);
    fclose(fromFile);
    fclose(toFile);
    
    return 0;
}

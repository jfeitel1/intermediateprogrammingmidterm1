#include "ppm_io.h"
#include "gray.h"
#include <stdlib.h>
#include <stdio.h>

Image * gray(Image *fimg) {
    Image * timg = (Image *) malloc (sizeof(Image));
    timg->rows = fimg->rows;
    timg->cols = fimg->cols;
    timg->data = (Pixel *) malloc(fimg->rows * fimg->cols * sizeof(Pixel));
    
    for (int i = 0; i < (fimg->rows * fimg->cols); i++) {
        int val = (fimg->data[i].r * 0.3) + (fimg->data[i].g * 0.59) + (fimg->data[i].b * 0.11);
        timg->data[i].r = val;
        timg->data[i].g = val;
        timg->data[i].b = val;
    }
    return timg;
}

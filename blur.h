#include "ppm_io.h"
#ifndef BLUR_H
#define BLUR_H

float sq(float f);
Image * blur(Image * fimg, float sigma);
void  gaussianFilter(float sigma, int N, float **filter);
Pixel* applyFilter(Pixel **imgMatrix, float **filter, int rows, int cols, int i, int j, int N);
#endif

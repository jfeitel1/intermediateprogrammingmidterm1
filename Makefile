# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CONSERVATIVE_FLAGS = -std=c99 -Wall -Wextra -pedantic 
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)


# TODO: Add a target to create the hw3 executable here
project: project.c swap.o bright.o invert.o gray.o crop.o blur.o edges.o ppm_io.o
	$(CC) project.c swap.o bright.o invert.o gray.o crop.o blur.o edges.o ppm_io.o -o project -lm

swap.o: swap.c swap.h
	$(CC) $(CFLAGS) -c swap.c

bright.o: bright.c bright.h
	$(CC) $(CFLAGS) -c bright.c

invert.o: invert.c invert.h
	$(CC) $(CFLAGS) -c invert.c

gray.o: gray.c gray.h
	$(CC) $(CFLAGS) -c gray.c

crop.o: crop.c crop.h
	$(CC) $(CFLAGS) -c crop.c

blur.o: blur.c blur.h
	$(CC) $(CFLAGS) -c  blur.c 

edges.o: edges.c edges.h
	$(CC) $(CFLAGS) -c edges.c

# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o project


// Jacob Feitelberg - jfeitel1
// Johnny Saldana - jsaldan1
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <stdio.h>

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
    assert(fp);
    
    Image *img = (Image *) malloc(sizeof(Image));
    char buffer[16];
    int maxColor;
    if(!fgets(buffer, sizeof(buffer), fp)) {
        // Couldn't read into the buffer text
        return NULL;
    }
    if(buffer[0] != 'P' || buffer[1] != '6') {
        // Incorrect type
        return NULL;
    }
    
    if(!img) {
        // Unable to allocate the memory
        return NULL;
    }
    
    // Check for comments and get rid of them
    int c = getc(fp);
    while(c == '#') {
        while(getc(fp) != '\n');
        c = getc(fp);
    }
    ungetc(c, fp);

    // Read in the image size
    if(fscanf(fp, "%d %d", &img->cols, &img->rows) != 2) {
        return NULL;
    }
    
    // Read in the color information
    if(fscanf(fp, " %d", &maxColor) != 1) {
        return NULL;
    }

    if(maxColor != 255) {
        return NULL;
    }
    while(fgetc(fp) != '\n');
    img->data = (Pixel *) malloc(img->rows * img->cols * sizeof(Pixel));
    if(!img) {
        return NULL;
    }
    fread(img->data, sizeof(Pixel), img->cols * img->rows, fp);
    return img;
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);
  
  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);
  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

